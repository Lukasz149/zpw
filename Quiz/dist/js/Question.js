"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Question = function Question(data) {
    _classCallCheck(this, Question);

    Object.assign(this, data);
};
//# sourceMappingURL=Question.js.map