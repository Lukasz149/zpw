let questionCounter = 0;
let questions = [];
let correctAnswerCounter = 0;
let wrongAnswerCounter = 0;
let noAnswerCounter = 0;
let interval;
$(document).ready(function() {
    loadJSON().done(function(response) {
        $(".quiz-result-wrapper").hide();
        for(let i = 0; i < response.length; i++) {
            questions.push(new Question(response[i]));
            let questionNumber = i + 1;
            $("<li><a href=\"#\">"+ questionNumber +"</a></li>").appendTo(".pagination");
        }
        questions = shuffle(questions);
        questions.forEach(v => {
            v.answers = shuffle(v.answers);
        });

        $('input[type="radio"]').on('click', function(e) {
            saveAnswer(this, questions);
        });
        showQuestion(questionCounter, questions);
        timeCounter();
    });
});


function loadJSON() {
    return $.getJSON('/data.json');
}


function saveAnswer(self, questions) {
    questions[questionCounter].userAnswer = parseInt(self.value);
    if(questions[questionCounter].userAnswer === questions[questionCounter].correctAnswer) {
        correctAnswerCounter++;
        setCorrectAnswerColor(questionCounter);
    } else {
        wrongAnswerCounter++;
        setWrongAnswerColor(questionCounter);
    }
    self.checked = false;
    ++questionCounter;
    changeQuestion(questionCounter, questions);

}

function changeQuestion(questionCounter, questions) {
    if(questionCounter === questions.length) {
        showResults(questions);
    } else {
        showQuestion(questionCounter, questions);
    }
}

function showQuestion(questionCounter, questions) {
    changeActiveQuestion(questionCounter);
    $("#question").text(questions[questionCounter].question);
    $("#ans1").val(questions[questionCounter].answers[0].id);
    $("#ans2").val(questions[questionCounter].answers[1].id);
    $("#ans3").val(questions[questionCounter].answers[2].id);
    $("#ans4").val(questions[questionCounter].answers[3].id);

    $("#answer1").text(questions[questionCounter].answers[0].answer);
    $("#answer2").text(questions[questionCounter].answers[1].answer);
    $("#answer3").text(questions[questionCounter].answers[2].answer);
    $("#answer4").text(questions[questionCounter].answers[3].answer);
    $(".progress-bar").attr("aria-valuenow", 20);
}

function showResults(questions) {
    $(".quiz-form").hide();
    $("#progress-bar").hide();
    $(".quiz-result-wrapper").show();
    $("li:last").removeClass("bg-info");
    let result = correctAnswerCounter/questions.length * 100;
    $(".quiz-result").text(result + "%");
    $(".correct-answer").text("Poprawnych odpowiedzi: " + correctAnswerCounter);
    $(".wrong-answer").text("Błędnych odpowiedzi: " + wrongAnswerCounter);
    $(".no-answer").text("Brak odpowiedzi: " + noAnswerCounter);
    clearInterval(interval);

    let userResult = {};
    userResult.date = new Date();
    userResult.result = result;
    userResult.correctAnswers = correctAnswerCounter;
    userResult.wrongAnswers = wrongAnswerCounter;
    userResult.noAnswers = noAnswerCounter;

    let results = localStorage.getItem("quizResults") === null ? [] : JSON.parse(localStorage.getItem("quizResults"));
    results.push(userResult);
    localStorage.setItem("quizResults", JSON.stringify(results));
    for(let i = 0; i< results.length; i++) {
        let lp = i + 1;
        let resultSummary = results[i].correctAnswers/questions.length * 100;
        $("<tr><th>" + lp + "</th>" +
            "<td>" + results[i].correctAnswers + "</td>" +
            "<td>" + results[i].wrongAnswers + "</td>" +
            "<td>" + results[i].noAnswers + "</td>" +
            "<td>" + resultSummary + "%</td></tr>").appendTo("tbody") ;

    }

}

function changeActiveQuestion(questionCounter) {
    let navs =  $('.pagination').children();
    if(questionCounter > 0) {
       $(navs[questionCounter-1]).find( "a" ).removeClass('bg-info');
    }
    $(navs[questionCounter]).find( "a" ).addClass('bg-info');
}

function setCorrectAnswerColor(questionCounter) {
    let navs =  $('.pagination').children();
    $(navs[questionCounter]).find("a").addClass('bg-success');
}

function setWrongAnswerColor(questionCounter) {
    let navs =  $('.pagination').children();
    $(navs[questionCounter]).find("a").addClass('bg-danger');
}

function setNoAnswerColor(questionCounter) {
    let navs =  $('.pagination').children();
    $(navs[questionCounter]).find("a").addClass('bg-secondary');
}

function shuffle(array) {
    let currentIndex = array.length, temporaryValue, randomIndex;
    while (0 !== currentIndex) {
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }
    return array;
}

function timeCounter() {
    interval = setInterval(timer ,1000);
    function timer() {
        let progressBar = $(".progress-bar");
        let nowTime = progressBar.attr("aria-valuenow");
        if(parseInt(nowTime) === 0) {
            setNoAnswerColor(questionCounter);
            noAnswerCounter++;
            ++questionCounter;
            changeQuestion(questionCounter, questions);
        } else {
            progressBar.attr("aria-valuenow", nowTime-1);
            progressBar.text(nowTime-1);
            let widthValue = 100 - 5 * (nowTime - 1);
            progressBar.css("width", widthValue + "%");
        }
    }
}
